const webpack = require("webpack");


module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
          videojs: 'video.js/dist/video.cjs.js',
          'window.videojs': 'video.js/dist/video.cjs.js',
      })
    ]
  }
}
