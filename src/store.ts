import Vue from 'vue';
import Vuex from 'vuex';
import { GetterTree, MutationTree, ActionTree } from 'vuex';
import moment from 'moment';
import axios, { AxiosError, AxiosResponse} from 'axios';

import { Gallery, Image, State } from './types';

Vue.use(Vuex);


function is_valid_image(image: Image) {
  // skip invalid images
  // TODO add log message...
  return image.width && image.height;
}


const getters: GetterTree<State, any> = {
  galleries: (state: State) => {
    return Object.values(state.galleries);
  },
  galleryById: (state: State) => {
    return (galleryId: string) => state.galleries[galleryId];
  },
  favoritesFor: (state: State) => {
    return (gallery: Gallery) => state.favorites[gallery.id];
  },
  itemsFor: (state: State) => {
    return (gallery: Gallery) => state.items[gallery.id];
  },
  isLoading: (state: State) => {
    return state.isLoading > 0;
  },
};

const mutations: MutationTree<State> = {
  SET_VIEW_TITLE: (state: State, title: string) => {
    state.viewTitle = title;
  },
  SET_TOTAL_GALLERIES: (state: State, total: number) => {
    state.totalGalleries = total;
  },
  SET_NEXT_GALLERY_PAGE: (state: State, page: number | null) => {
    state.nextPage = page;
  },
  SET_GALLERY_DETAIL: (state: State, gallery: Gallery) => {
    Vue.set(
      state.galleries, gallery.id,
      {
        ...gallery,
        date: moment(gallery.date, 'YYYY.MM.DD'),
      },
    );
  },
  SET_GALLERY_FAVS: (state: State, { galleryId, images }) => {
    Vue.set(state.favorites, galleryId, images.filter(is_valid_image));
  },
  SET_GALLERY_ITEMS: (state: State, { galleryId, images }) => {
    Vue.set(state.items, galleryId, images.filter(is_valid_image));
  },
  ADD_GALLERY_ITEM: (state: State, { galleryId, image }) => {
    if (state.items[galleryId].indexOf(image) < 0) {
      state.items[galleryId].push(image);
    }
  },
  ERROR_MESSAGE: (state: State, message: string) => {
    state.error = message;
  },
  LOADING: (state: State, isLoading: boolean) => {
    if (isLoading === true) {
      state.isLoading += 1;
    } else {
      state.isLoading -= 1;
    }
  },
};

const actions: ActionTree<State, any> = {
  FETCH_GALLERY_LIST: async ({ commit, dispatch }, page: number | null) => {
    if (page === null) {
      return;
    }
    const url = `${process.env.VUE_APP_API_BASE_URL}/galleries/?page=${page}`;
    const response = await axios.get(url);

    if (response.data.next !== null) {
      const nextPage = (new URL(response.data.next)).searchParams.get('page');
      commit('SET_NEXT_GALLERY_PAGE', nextPage);
    } else {
      commit('SET_NEXT_GALLERY_PAGE', null);
    }
    commit('SET_TOTAL_GALLERIES', response.data.count);
    for (const gallery of response.data.results) {
      commit('SET_GALLERY_DETAIL', gallery);
      dispatch('FETCH_GALLERY_FAVS', gallery.id);
    }
  },
  FETCH_GALLERY_DETAIL: async ({ commit }, galleryId: string) => {
    const url = `${process.env.VUE_APP_API_BASE_URL}/galleries/${galleryId}/`;
    const response = await axios.get(url);
    commit('SET_GALLERY_DETAIL', response.data);
  },
  FETCH_GALLERY_FAVS: async ({ commit }, galleryId: string) => {
    const url = `${process.env.VUE_APP_API_BASE_URL}/galleries/${galleryId}/random/`;
    const response = await axios.get(url);
    commit('SET_GALLERY_FAVS', { galleryId, images: response.data });
  },
  FETCH_GALLERY_ITEMS: async ({ commit }, galleryId: string) => {
    const url = `${process.env.VUE_APP_API_BASE_URL}/galleries/${galleryId}/items/`;
    const response = await axios.get(url);
    commit('SET_GALLERY_ITEMS', { galleryId, images: response.data });
  },
  ADD_ITEM_TO_GALLERY: async ({ commit }, { fileId, galleryId }) => {
    const url = `${process.env.VUE_APP_API_BASE_URL}/galleries/${galleryId}/add_uploaded_item/`;
    const payload = {
      file_id: fileId,
    };
    const response = await axios.post(url, payload);
    // TODO response might not be ready, as thumbnails and videos are created
    //      in an async manner
    commit('ADD_GALLERY_ITEM', { galleryId, image: response.data });
  },
};

const initialState: State = {
  error: null,
  isLoading: 0,
  galleries: {},
  favorites: {},
  items: {},
  viewTitle: null,
  totalGalleries: 0,
  nextPage: 1,
};

export default new Vuex.Store<State>({
  state: {...initialState},
  actions,
  mutations,
  getters,
});
