import Vue from 'vue';
import Router from 'vue-router';

import GalleryList from './views/GalleryList.vue';
import GalleryDetail from './views/GalleryDetail.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'gallery-list',
      component: GalleryList,
    },
    {
      path: '/gallery/:galleryId',
      name: 'gallery-detail',
      component: GalleryDetail,
    },
  ],
});
