/*
 * justified-layout
 */
interface Box {
  aspectRadio: number;
  top: number;
  width: number;
  height: number;
  left: number;
}

interface Layout {
  containerHeight: number;
  widowCount: number;
  boxes: Box[];
}

interface ImageSize {
  width: number;
  height: number;
}

interface Padding {
  top: number;
  left: number;
  right: number;
  bottom: number;
}

interface Spacing {
  horizontal: number;
  vertical: number;
}

interface LayoutConfig {
  containerWidth?: number;
  containerPadding?: number | Padding;
  boxSpacing?: number | Spacing;
  targetRowHeight?: number;
  targetRowHeightTolerance?: number;
  maxNumRows?: number;
  forceAspectRatio?: number | boolean;
  showWidows?: boolean;
  fullWidthBreakoutRowCadence?: number | boolean;
}

declare module 'justified-layout' {
  function justifyLayout(input: ImageSize[] | number[], config?: LayoutConfig): Layout;
  export = justifyLayout;
}


/*
 * vue-resize-directive
 */
declare module 'vue-resize-directive' {
  function insert(el: HTMLElement): void;
  export default insert;
}


/*
 * vue-images-loaded
 */
declare module 'vue-images-loaded' {
  export function bind(el: HTMLElement): void;
  export function inserted(el: HTMLElement, binding: any): void;
  export function componentUpdated(el: HTMLElement, binding: any): void;
  export function unbind(el: HTMLElement, binding: any): void;
}


/*
 * vuew-simple-spinner
 */
declare module 'vue-simple-spinner' {
  import Vue from 'vue';
  export default Vue;
}


/*
 * vue-gallery
 */
declare module 'vue-gallery';


/*
 * vue-moment
 */
declare module 'vue-moment';



/*
 * vue-infinite-scroll
 */
declare module 'vue-infinite-scroll';
