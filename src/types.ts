import moment from 'moment';

export interface Format {
  height: number;
  mime_type: string;
  codecs: string;
  url: string;
  width: number;
}

export interface Image {
  id: number;
  gallery_id: string;
  type: string;
  original_filename: string;
  original: string;
  thumbnails: {[key: string]: string};
  width: number;
  height: number;
  formats?: Format[];
  duration?: number;
}


export interface Gallery {
  id: string;
  title: string;
  slug: string;
  owner: string;
  description: string;
  date: moment.Moment;
}


export interface State {
  error: string | null;
  isLoading: number;
  galleries: {[key: string]: Gallery};
  favorites: {[key: string]: Image[]};
  items: {[key: string]: Image[]};
  viewTitle: string | null;
  totalGalleries: number;
  nextPage: number | null;
}
