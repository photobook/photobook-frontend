import axios, { AxiosError, AxiosResponse} from 'axios';
import Vue from 'vue';
import App from './App.vue';
import Moment from 'vue-moment';
import router from './router';
import store from './store';


axios.interceptors.request.use(
  (config) => {
    store.commit('ERROR_MESSAGE', null);
    store.commit('LOADING', true);
    return config;
  },
  (error) => {
    store.commit('LOADING', false);
    return Promise.reject(error);
  },
);
axios.interceptors.response.use(
  (response) => {
    store.commit('LOADING', false);
    return response;
  },
  (error) => {
    store.commit('LOADING', false);
    if (!error.status) {
      store.commit(
        'ERROR_MESSAGE',
        'Connection to server failed... reload to try again.',
      );
    }
    return Promise.reject(error);
  },
);


Vue.use(Moment);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
